#!/usr/bin/env python3

from operator import ne
import matplotlib.pyplot as plt
import numpy as np
import os
from sklearn.svm import SVC
import read_pics
import var_fact
from random import shuffle

def machine_learning(table):
    target_train = [''] * 43
    input_train = []
    for i in range(len(table)):
        if len(table[i]) > 0:
            target_train[i] = table[i][0]
        if len(table[i]) == 2: 
            input_train.insert(-1, table[i][1][0][0])
    new_input_train = []
    new_target_train = []
    for i in range(len(table)):
        name = table[i][0]
        for j in range(len(table[i][1][0])):
            new_target_train.insert(-1, name)
            new_input_train.insert(-1, table[i][1][0][j])
    test = read_pics.get_pics_from_file('../' + "pics_LOGINMDP.bin")[0]
    new_list = [test[i] for i in range(len(test))]
    S = SVC()
    S.fit(new_input_train, new_target_train)
    list_predicted = S.predict(new_list)
    print(clean_list(list_predicted))

def clean_list(list):
    new_list = []
    precedent = None
    for k in range(len(list)):
        if precedent == None or precedent != list[k]:
            new_list.append(list[k])
            precedent = list[k]
    return new_list
            

def create_table():
    table = []
    for filename in os.listdir('../data'):
        table.insert(-1, [filename, read_pics.get_pics_from_file('../data/' + filename)])
    resultat = machine_learning(table)
    print(resultat)

if __name__ == "__main__":
    create_table()
