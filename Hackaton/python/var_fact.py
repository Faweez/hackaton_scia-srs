#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import os
import read_pics

def table_var_fact():
    table = [] * 41
    for filename in os.listdir('../data'):
        file = open('../data/' + filename, "rb")
        pics = [(0, 0)] * 17
        pic = read_pics.read_double_tab(file, 17)
        nominal = read_pics.read_double_tab(open('../data/pics_NOKEY.bin', "rb"), 17)
        for i in range(len(pic)):
            pics[i] = (pic[i], pic[i] - nominal[i])
        table.append((filename, pics))
    print(table)
    return table


if __name__ == "__main__":
    table_var_fact()


